import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import { useNavigation, useRoute } from '@react-navigation/native'; 

const TodoDetails = () => {
  const navigation = useNavigation(); // Importante
  const route = useRoute(); // Importante

  // Desestruturar o objeto 'task':
  const { task } = route.params;

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{task.title}</Text>
      <Text>Data: {task.date}</Text>
      <Text>Hora: {task.time}</Text>
      {/* Exiba outros detalhes da task aqui, se tiver */}
    </View>
  );
};

export default TodoDetails;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center', // Se quiser centralizar
    justifyContent: 'center', // Se quiser centralizar
    padding: 20, 
  },
  title: {
    fontSize: 22,
    fontWeight: 'bold',
    marginBottom: 10,
  }
});
