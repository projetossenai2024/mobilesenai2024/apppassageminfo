import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    FlatList} from 'react-native'
  import { useNavigation } from '@react-navigation/native'; 
import React from 'react'

const TodoList = () => {
    const navigation = useNavigation();
    const tasks = [
        {
          id: 1,
          title: "Ir ao supermercado",
          date: "2024-02-27",
          time: "10h",
          address: "Rua Estevão Leão Bouroul"
        },
        {
          id: 2,
          title: "Reunião com a equipe",
          date: "2024-02-28",
          time: "14h",
          location: "Sala de Reuniões - Prédio 2"
        },
        {
          id: 3,
          title: "Fazer check-up médico",
          date: "2024-03-01",
          time: "9h",
          location: "Clínica Dr. Carlos",
          appointment: "123456"
        },
        {
          id: 4,
          title: "Pagar contas",
          date: "2024-03-02",
          time: "10h",
          description: "Pagar conta de luz, água e internet"
        },
        {
          id: 5,
          title: "Assistir aula online",
          date: "2024-03-03",
          time: "19h",
          link: "https://www.aula.education/"
        },
        {
          id: 6,
          title: "Fazer compras online",
          date: "2024-03-04",
          time: "20h",
          website: "https://lojaintegrada.com.br/"
        },
        {
          id: 7,
          title: "Levar o carro para a oficina",
          date: "2024-03-05",
          time: "9h",
          location: "Oficina Mecânica XYZ"
        },
        {
          id: 8,
          title: "Encontrar com amigos",
          date: "2024-03-06",
          time: "20h",
          location: "Bar do Zé"
        },
        {
          id: 9,
          title: "Fazer faxina na casa",
          date: "2024-03-07",
          time: "10h",
          description: "Limpar sala, cozinha, quartos e banheiros"
        },
        {
          id: 10,
          title: "Relaxar e descansar",
          date: "2024-03-08",
          time: "Dia livre",
          description: "Ler um livro, assistir um filme, fazer um piquenique no parque"
        }
    ];
    
const taskPress=(task) =>{
    navigation.navigate('TodoDetails', {task})
}

    return (
        <View style={styles.container}>
            <FlatList
                data={tasks}
                keyExtractor={(item) => item.id.toString()}
                renderItem={({item})=>(
                    <View style={styles.list}>
                        <TouchableOpacity onPress={() => taskPress(item)}>
                            <Text style={styles.textContainer}>{item.title}</Text>
                        </TouchableOpacity>
                    </View>
                )}
            />
        </View>
    )
}

export default TodoList

const styles = StyleSheet.create({
  container:{
    flex: 1,
    alignItems: 'center',
    marginTop: 35,
  },
  textUser:{
      fontSize: 24,
      fontWeight: 'bold',
      marginBottom: 5,
  },
  textContainer:{
      backgroundColor: '#894de9',
      margin: 5,
      borderRadius: 8,
      fontSize: 16,
      fontWeight: '800',
      padding: 5
  }

})